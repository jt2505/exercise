use officedb;
create table product(product_id int  , product_name varchar(30), primary key(product_id));
create table category(category_id int , category_name varchar(30), primary key(category_id));
create table product_category(product_to_category_id int auto_increment ,productid int ,foreign key(productid) references product(product_id),categoryid int,foreign key(categoryid) references category(category_id), primary key(product_to_category_id));

