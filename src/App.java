import dao.CategoryDao;
import dao.ProductDao;
import database.Database;
import models.Category;
import pagination.Page;
import models.Product;
import services.ProductService;

import java.sql.Connection;
import java.util.List;

public class App {
    public static void main(String[] args) {
        Connection dbconn;
        dbconn = Database.getConnection();
        //Read Product Table via pagination
        ProductDao pDao = new ProductDao();
        System.out.println("\nReading Product Table via pagination \n\n");
        List<Product> list;
        Page<Product> pg = new Page<>();
        pg.setPageNo(1);
        pg.setPageSize(4);
        list = pDao.getRecordsPerPage(pg);
        for (Product product : list) {
            System.out.println(product.getId() + " " + product.getName());
        }


        //Read Product table
        ProductService pServ = new ProductService();
        list=pServ.readTable();
        //ProductDao ppDao = new ProductDao(dbconn);
        System.out.println("\n\nReading Product Table\n\n");
        for (Product product : list) {
            System.out.println(product.getId() + " " + product.getName());
        }

        //Read Category table
        CategoryDao cDao = new CategoryDao();
        System.out.println("\n\nReading Category Table\n\n");
        List<Category> list1;
        list1 = cDao.readTable();
        for (Category category : list1) {
            System.out.println(category.getId() + " " + category.getName());
        }


        //Inserting data in product table
        Product p = new Product();
        p.setId(12);
        p.setName("product12");
        pServ.save(p);
        System.out.println("\n\nGetting list of categories of specific product\n\n");
        Product p1 = new Product();
        p1.setId(1);
        //p1.setName("shirt");
        //p1.getCategoryByProduct(dbconn, p1);
        System.out.println("\n\nInserted into product table\n\n");
        list = pServ.readTable();
        for (Product product : list) {
            System.out.println(product.getId() + " " + product.getName());
        }

        //Inserting data in category table
        Category c = new Category();
        c.setId(11);
        c.setName("rajvi's");
        cDao.save(c);
        System.out.println("\n\nInserted into category table\n\n");
        list1 = cDao.readTable();
        for (Category category : list1) {
            System.out.println(category.getId() + " " + category.getName());
        }

        //Delete data from product table
        //pDao.deleteData(pDao.get(10));
        System.out.println("\n\nDeleted product table\n\n");
        Product p2=new Product();
        p2.setId(3);
        pServ.deleteData(p2);
        list=pServ.readTable();
        for (Product product : list) {
            System.out.println(product.getId() + " " + product.getName());
        }

        //Delete data from category table
        cDao.deleteData(cDao.get(10));
        System.out.println("\n\nDeleted category table\n\n");
        list1 = cDao.readTable();
        for (Category category : list1) {
            System.out.println(category.getId() + " " + category.getName());
        }

        //Update data of product table
        Product pd=pServ.get(8);
        if (pd != null) {
            pd.setName("tshirt");
            pServ.updateData(pd);
            System.out.println("\n\nUpdated product table\n\n");
            list = pServ.readTable();
            for (Product product : list) {
                System.out.println(product.getId() + " " + product.getName());
            }
        }

        //Update data of category table
        Category cd = cDao.get(5);
        if (cd != null) {
            cd.setName("reading");
            cDao.updateData(cd);
            System.out.println("\n\nUpdated category table\n\n");
            list1 = cDao.readTable();
            for (Category category : list1) {
                System.out.println(category.getId() + " " + category.getName());
            }
        }

        //Link Product with Category
        Product p3 = pDao.get(11);
        Category c3 = cDao.get(3);
        cDao.linkProduct(c3, p3);

        //list all product of given category
        Category c4 = cDao.get(3);
        List<String> str = pDao.getbyCategory(c4);
        System.out.println("\n\nProducts By Category\n\n");
        str.forEach(System.out::println);
    }
}
