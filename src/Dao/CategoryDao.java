package dao;

import database.Database;
import models.Category;
import pagination.Page;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDao implements Dao<Category> {
    private final Connection conn;

    public CategoryDao() {
        this.conn = Database.getConnection();
    }

    @Override
    public List<Category> getAll() {

        String query = "select * from category";
        List<Category> list = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt(1));
                c.setName(rs.getString(2));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println("error");
        }
        return list;
    }

    public int count() {
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select count(category_id) from category");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("Error, counting total rows from category table");
        }
        return 0;
    }

    @Override
    public void save(Category c) {
        try {
            PreparedStatement pstmt = conn.prepareStatement("insert ignore into category(category_name) values (?)");
            pstmt.setString(1, c.getName());
            pstmt.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Error, can't insert value in category table");
        }
    }

    @Override
    public void delete(Category c) {
        try {
            Category c1;
            c1=get(c);
            unlink(c1);
            PreparedStatement pstmt = conn.prepareStatement("delete from category where category_name=?");
            pstmt.setString(1,c.getName());
            pstmt.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Error, can't delete value from category table");
        }

    }

    @Override
    public Category get(Category category) {
        Category c=new Category();
        try {
            PreparedStatement pstmt= conn.prepareStatement("select category_id from category where category_name=?");
            pstmt.setString(1, category.getName());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                c.setId(rs.getInt(1));
                break;
            }
        } catch (SQLException s) {
            System.out.println("Error while fetching product details");
        }
        return c;
    }

    @Override
    public void update(String old_name,Category c) {
        try {
            PreparedStatement pstmt = conn.prepareStatement("update category set category_name=? where category_name=?");
            pstmt.setString(1, c.getName());
            pstmt.setString(2,old_name);
            pstmt.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Error, cant update category table");
        }
    }

    public List<Category> getRecordsPerPage(Page pg) {
        List<Category> list = new ArrayList<>();
        try {
            PreparedStatement pstmt = conn.prepareStatement("select * from category limit ? offset ? ");
            pstmt.setInt(1, pg.getPageSize());
            pstmt.setInt(2, (pg.getPageNo() - 1) * pg.getPageSize());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt(1));
                c.setName(rs.getString(2));
                list.add(c);
            }
        } catch (SQLException s) {
            System.out.println("Error, paging the category table ");
        }
        return list;
    }

    public void unlink(Category c) {
        try {
            PreparedStatement pstmt = conn.prepareStatement("delete from product_category where categoryid=?");
            pstmt.setInt(1, c.getId());
            pstmt.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Linked product deleted");
        }
    }
}
