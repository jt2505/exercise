package dao;

import java.util.List;

public interface Dao<T> {
    List<T> getAll();

    void save(T t);

    void delete(T t);

    T get(T t);

    void update(String s,T t);

    void unlink(T t);


}
