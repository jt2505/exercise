package dao;

import database.Database;
import models.Category;
import models.Product;
import pagination.Page;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDao implements Dao<Product> {
    private final Connection conn;

    public ProductDao() {
        this.conn = Database.getConnection();
    }

    @Override
    public List<Product> getAll() {
        List<Product> list = new ArrayList<>();
        String query = "select * from product";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                list.add(p);
            }
        } catch (SQLException s) {
            System.out.println("Error in product table");
        }
        return list;
    }

    public int count() {
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select count(product_id) from product");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("Error, counting total rows from product table");
        }
        return 0;
    }

    @Override
    public void save(Product p) {

        try {
            PreparedStatement pstmt = conn.prepareStatement("insert ignore into product(product_name) values (?)");
            pstmt.setString(1, p.getName());
            pstmt.executeUpdate();
            Product p1;
            p1=get(p);
            p1.setCategories(p.getCategories());
            p1.setName(p.getName());
            for(int i=0;i<p1.getCategories().size();i++)
            {
             if(p1.getCategories().size()!=0)
             {
                 Category c=new Category();
                 c.setId(p1.getCategories().get(i).getId());
                 linkCategoriesOfProduct(c,p1);
             }
            }
        } catch (SQLException s) {
            System.out.println("Error, can't insert value in product table");
        }
    }

    @Override
    public void delete(Product p) {
        try {
            Product p1;
            p1=get(p);
            unlink(p1);

            PreparedStatement pstmt = conn.prepareStatement("delete from product where product_name=?");
            pstmt.setString(1,p.getName());
            pstmt.executeUpdate();
        } catch (SQLException s) {
            System.out.println("Error, can't delete value from product table");
        }

    }

    @Override
    public Product get(Product product) {
        Product p=new Product();
        try {
            PreparedStatement pstmt= conn.prepareStatement("select product_id from product where product_name=?");
            pstmt.setString(1, product.getName());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                p.setId(rs.getInt(1));
                break;
            }
        } catch (SQLException s) {
            System.out.println("Error while fetching product details");
        }
        return p;
    }

    @Override
    public void update(String old_name,Product p) {
        Product p1=new Product();
        p1.setName(old_name);
        p1=get(p1);
        unlink(p1);
        try {
            PreparedStatement pstmt = conn.prepareStatement("update product set product_name=? where product_name=?");
            pstmt.setString(1, p.getName());
            pstmt.setString(2,old_name);
            pstmt.executeUpdate();
            p1.setCategories(p.getCategories());
            if(p1.getCategories().size()!=0) {
                System.out.println("this will never be called");
                for (int i = 0; i < p1.getCategories().size(); i++) {
                    Category c = new Category();
                    c.setId(p1.getCategories().get(i).getId());
                    linkCategoriesOfProduct(c, p1);
                }
            }
        } catch (SQLException s) {
            System.out.println("Error, cant update product table");
        }
    }

    public void linkCategoriesOfProduct(Category c,Product p) {
        try{
            PreparedStatement pstmt= conn.prepareStatement("insert into product_category(productid,categoryid) values (?,?)");
            pstmt.setInt(1,p.getId());
            pstmt.setInt(2,c.getId());
            pstmt.executeUpdate();
        }catch (SQLException se) {
            System.out.println(String.format("SQL Error while linking product. Product ID %d Category ID %d. Error: %s", p.getId(), c.getId(), se.getMessage()));

        }
    }

    public List<Product> getRecordsPerPage(Page pg) {
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement pstmt = conn.prepareStatement("select * from product limit ? offset ? ");
            pstmt.setInt(1, pg.getPageSize());
            pstmt.setInt(2, (pg.getPageNo() - 1) * pg.getPageSize());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                list.add(p);
            }
        } catch (SQLException s) {
            System.out.println("Error, paging the product table ");
        }
        return list;
    }

    public List<Category> fetchCategoriesOfGivenProduct(int id) {
        List<Category> category = new ArrayList<>();
        try {
            PreparedStatement pstmt = conn.prepareStatement("select category.category_name from category " +
                    "inner join product_category on category.category_id=product_category.categoryid" +
                    " inner join product on product.product_id=product_category.productid and" +
                    " product.product_id=?");
            pstmt.setInt(1,id);
            ResultSet rs=pstmt.executeQuery();
            while (rs.next()) {
                Category c = new Category();
                c.setName(rs.getString(1));
                category.add(c);
            }
        } catch (SQLException se) {
            System.out.println("Error, can't find category list of given product");
        }
        return category;
    }

    public void unlink(Product p){
        try{
            PreparedStatement pstmt= conn.prepareStatement("delete from product_category where productid=?");
            pstmt.setInt(1,p.getId());
            pstmt.executeUpdate();
        }catch(SQLException s){
            System.out.println("Linked product deleted");
        }
    }

}
