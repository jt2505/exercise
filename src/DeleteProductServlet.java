import models.Product;
import services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/DeleteProductServlet")
public class DeleteProductServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // read form fields
        String p_name = request.getParameter("product_name");
        Product p=new Product();
        p.setName(p_name);
        ProductService pServ=new ProductService();
        pServ.delete(p);

        // get response writer
        PrintWriter writer = response.getWriter();

            // build HTML code
            String htmlRespone = "<html>";
            htmlRespone += "<h2>Product " + p_name;
            htmlRespone += " Deleted Successfully </h2>";
            htmlRespone += "<head><body></br></br>";
            htmlRespone += "<a class=\"dropdown-item\" href=\"products.jsp\">Display Product Table</a></body></head>";
            htmlRespone += "</html>";

        // return response
        writer.println(htmlRespone);

    }

}
