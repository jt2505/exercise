package models;

public interface Table {

    int getId();

    void setId(int id);

    String getName();

    void setName(String name);
}
