import models.Category;
import services.CategoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/SaveCategoryServlet")
public class SaveCategoryServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // read form fields
        String c_name = request.getParameter("category_name");
        Category c=new Category();
        c.setName(c_name);
        CategoryService cServ=new CategoryService();
        cServ.save(c);

        // get response writer
        PrintWriter writer = response.getWriter();

        // build HTML code
        String htmlRespone = "<html>";
        htmlRespone += "<h2>Category " + c_name;
        htmlRespone += "  Successfully Registered</h2>";
        htmlRespone += "<head><body></br></br>";
        htmlRespone+= "<a class=\"dropdown-item\" href=\"categories.jsp\">Display Category Table</a></body></head>";
        htmlRespone += "</html>";

        // return response
        writer.println(htmlRespone);

    }

}
