import models.Category;
import models.Product;
import services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/SaveProductServlet")
public class SaveProductServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // read form fields
        String p_name = request.getParameter("product_name");
        Product p = new Product();
        p.setName(p_name);
        String[] categories = request.getParameterValues("categories");
        List<Category> categoryList = new ArrayList<>();
        for (String id : categories) {
            Category c = new Category();
            c.setId(Integer.parseInt(id));
            categoryList.add(c);
        }
        p.setCategories(categoryList);

        ProductService pServ = new ProductService();
        pServ.save(p);


        // get response writer
        PrintWriter writer = response.getWriter();
            // build HTML code
            String htmlRespone = "<html>";
            htmlRespone += "<h2>Product " + p_name;
            htmlRespone += "  Successfully Registered</h2>";
            htmlRespone += "<head><body></br></br>";
            htmlRespone += "<a class=\"dropdown-item\" href=\"products.jsp\">Display Product Table</a></body></head>";
            htmlRespone += "</html>";


        // return response
        writer.println(htmlRespone);

    }

}
