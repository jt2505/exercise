import models.Category;
import models.Product;
import services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/UpdateProductServlet")
public class UpdateProductServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // read form fields
        String old_name = request.getParameter("old_name");
        String new_name = request.getParameter("new_name");
        Product p=new Product();
        p.setName(new_name);
        String[] categories = request.getParameterValues("categories");
        List<Category> categoryList = new ArrayList<>();
        for (String id : categories) {
            Category c = new Category();
            c.setId(Integer.parseInt(id));
            categoryList.add(c);
        }
        p.setCategories(categoryList);
        ProductService pServ=new ProductService();
        pServ.update(old_name,p);

        // get response writer
        PrintWriter writer = response.getWriter();

        // build HTML code
        String htmlRespone = "<html>";
        htmlRespone += "<h2>Product "+old_name;
        htmlRespone += " renamed Successfully as "+ new_name+ " </h2>";
        htmlRespone += "<head><body></br></br>";
        htmlRespone+= "<a class=\"dropdown-item\" href=\"products.jsp\">Display Product Table</a></body></head>";
        htmlRespone += "</html>";

        // return response
        writer.println(htmlRespone);

    }

}
