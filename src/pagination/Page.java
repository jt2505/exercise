package pagination;


import java.util.List;

public class Page<T> {
    private int pageNumber, pageSize;
    private int total;
    private List<T> records;

    public int getPageNo() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageNo(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public List<T> getRecords() {
        return records;
    }

    public int getTotalPages() {
        int totalPages = total / pageSize;
        return total % pageSize == 0 ? totalPages : totalPages + 1;
    }
}
