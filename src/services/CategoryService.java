package services;

import dao.CategoryDao;
import models.Category;
import pagination.Page;

import java.util.List;

public class CategoryService extends Service<Category> {

    private final CategoryDao cDao;

    public CategoryService() {
        cDao = new CategoryDao();
    }

    public Page<Category> getPage(int pageNo, int pageSize) {
        Page<Category> pg = new Page<>();
        pg.setPageNo(pageNo);
        pg.setPageSize(pageSize);
        pg.setRecords(cDao.getRecordsPerPage(pg));
        pg.setTotal(cDao.count());
        return pg;
    }

    @Override
    public List<Category> getAll() {
        return cDao.getAll();
    }

    @Override
    public void delete(Category category) {
        cDao.delete(category);
    }

    @Override
    public void update(String old_name, Category category) {
        cDao.update(old_name, category);
    }

    @Override
    public void save(Category category) {
        cDao.save(category);
    }

    /*@Override
    public int get(String string) {
        return cDao.get(string);
    }*/
}
