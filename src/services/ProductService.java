package services;

import dao.ProductDao;
import models.Category;
import models.Product;
import pagination.Page;

import java.util.List;

public class ProductService extends Service<Product> {

    private final ProductDao pDao;
    public ProductService() {
        pDao = new ProductDao();
    }

    public Page<Product> getPage(int pageNo, int pageSize) {
        Page<Product> pg = new Page<>();
        pg.setPageNo(pageNo);
        pg.setPageSize(pageSize);
        pg.setRecords(pDao.getRecordsPerPage(pg));
        pg.setTotal(pDao.count());
        return pg;
    }

    @Override
    public List<Product> getAll() {
        return pDao.getAll();
    }

    @Override
    public void delete(Product product) {
            pDao.delete(product);
    }

    @Override
    public void save(Product product) {
            pDao.save(product);
    }

    @Override
    public void update(String old_name,Product product) {
            pDao.update(old_name,product);
    }

   /* @Override
    public int get(String string) {
        return pDao.get(string);
    }*/
    public List<Category> fetchCategoriesOfGivenProduct(int id){
        return pDao.fetchCategoriesOfGivenProduct(id);
    }

}
