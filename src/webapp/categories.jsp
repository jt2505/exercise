<%@ page import="services.CategoryService" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Category" %>
<%@ page import="pagination.Page" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Categories</title>
</head>
<body>
<div class="container">
    <%
        CategoryService categoryService = new CategoryService();
        int currPageNum = 1;
        try {
            currPageNum = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
        }
        Page<Category> currPage = categoryService.getPage(currPageNum, 5);
        List<Category> categories = currPage.getRecords();
    %>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Category category : categories) {
                out.println("<tr>");
                out.println(String.format("<td scope=\"row\">%d</td>", category.getId()));
                out.println(String.format("<td>%s</td>", category.getName()));
                out.println("</tr>");
            }
        %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <%
                int totalPages = currPage.getTotalPages();

                if (currPageNum != 1)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Previous</a></li>", currPageNum - 1));
                for (int i = 0; i < totalPages; i++) {
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">%d</a></li>", i + 1, i + 1));
                }
                if (currPageNum != totalPages)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Next</a></li>", currPageNum + 1));
            %>

        </ul>
    </nav>

</div>
</body>
</html>
