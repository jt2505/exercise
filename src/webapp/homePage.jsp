<%@ page import="services.CategoryService" %>
<%@ page import="services.ProductService" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Category" %>
<%@ page import="models.Product" %>
<%@ page import="pagination.Page" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<style type="text/css">
.custom {
	font-family: Courier;
	color: #005CB9;
	font-size:20px;
}
</style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Home Page</title>
</head>
<body>
<div class="container">
        <div align="center"><span class="custom"><font style="text-decoration: underline;"><h3>Product</h3></font></span></div>
    <%
        ProductService productService = new ProductService();
        int currPageNum1 = 1;
        try {
            currPageNum1 = Integer.parseInt(request.getParameter("page1"));
        } catch (NumberFormatException e) {
        }
        Page<Product> currPage1 = productService.getPage(currPageNum1, 5);
        List<Product> products = currPage1.getRecords();
    %>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">PRODUCT_ID</th>
            <th scope="col">PRODUCT_NAME</th>
            <th scope="col">List_Of_Categories</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Product product : products) {
            out.println("<tr>");
            out.println(String.format("<td scope=\"row\">%d</td>", product.getId()));
            out.println(String.format("<td>%s</td>", product.getName()));
        %>
                        <td>
                        <%
                         List<Category> list=productService.fetchCategoriesOfGivenProduct(product.getId());
                         if(list!=null){
                         for(int i=0;i<list.size();i++){
                                if(i==(list.size()-1)){
                                out.print(list.get(i).getName()+".");
                                }
                                else{
                              out.print(list.get(i).getName()+", ");
                              }
                              }
                         }
                         %>
                        </td>
                        <%
                        out.println("</tr>");
                    }
                %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <%
                int totalPages1 = currPage1.getTotalPages();

                if (currPageNum1 != 1)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page1=%d\">Previous</a></li>", currPageNum1 - 1));
                for (int i = 0; i < totalPages1; i++) {
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page1=%d\">%d</a></li>", i + 1, i + 1));
                }
                if (currPageNum1 != totalPages1)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page1=%d\">Next</a></li>", currPageNum1 + 1));
            %>

        </ul>
    </nav>
    </br></br>
    <div align="center"><span class="custom"><font style="text-decoration: underline;"><h3>Category</h3></font></span></div>
    <%
        CategoryService categoryService = new CategoryService();
        int currPageNum = 1;
        try {
            currPageNum = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
        }
        Page<Category> currPage = categoryService.getPage(currPageNum, 5);
        List<Category> categories = currPage.getRecords();
    %>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">CATEGORY_ID</th>
            <th scope="col">CATEGORY_NAME</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Category category : categories) {
                out.println("<tr>");
                out.println(String.format("<td scope=\"row\">%d</td>", category.getId()));
                out.println(String.format("<td>%s</td>", category.getName()));
                out.println("</tr>");
            }
        %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <%
                int totalPages = currPage.getTotalPages();

                if (currPageNum != 1)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Previous</a></li>", currPageNum - 1));
                for (int i = 0; i < totalPages; i++) {
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">%d</a></li>", i + 1, i + 1));
                }
                if (currPageNum != totalPages)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Next</a></li>", currPageNum + 1));
            %>

        </ul>
    </nav>
</div>
</body>
</html>
